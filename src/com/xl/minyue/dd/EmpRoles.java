package com.xl.minyue.dd;

public class EmpRoles {
	private String roleId;
	private String roleName;
	private String groupName;

	public EmpRoles() {
		// TODO Auto-generated constructor stub
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "EmpRoles [roleId=" + roleId + ", roleName=" + roleName + ", groupName=" + groupName + "]";
	}
	
	

}
