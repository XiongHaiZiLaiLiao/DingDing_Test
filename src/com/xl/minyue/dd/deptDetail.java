package com.xl.minyue.dd;

public class deptDetail {
	private String deptId;
	private String deptName;
	private String parentId;
	private String deptMan;
	
	public deptDetail() {
		// TODO Auto-generated constructor stub
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getDeptMan() {
		return deptMan;
	}
	public void setDeptMan(String deptMan) {
		this.deptMan = deptMan;
	}
	@Override
	public String toString() {
		return "deptDetail [deptId=" + deptId + ", deptName=" + deptName + ", parentId=" + parentId + ", deptMan="
				+ deptMan + "]";
	}
	
	
}
