package com.xl.minyue.dd;

import java.util.ArrayList;
import java.util.Arrays;

public class Emp {
	private String userId;
	private String userName;
	private ArrayList deptIdList;
	private String position;
	private Boolean isSenior;
	private Boolean isBoss;
	private String avatar;
	private String email;
	private ArrayList<EmpRoles> roleArray =new ArrayList<>();
	private Boolean isAdmin;
	private String mobile;
	private Boolean isLeaderInDepts;//该人员在部门中是否是主管
	private ArrayList leaderDeptIdList;//管理的部门列表
	
	public Emp() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Boolean getIsLeaderInDepts() {
		return isLeaderInDepts;
	}


	public void setIsLeaderInDepts(Boolean isLeaderInDepts) {
		this.isLeaderInDepts = isLeaderInDepts;
	}


	public ArrayList getLeaderDeptIdList() {
		return leaderDeptIdList;
	}


	public void setLeaderDeptIdList(ArrayList leaderDeptIdList) {
		this.leaderDeptIdList = leaderDeptIdList;
	}


	public ArrayList getDeptIdList() {
		return deptIdList;
	}


	public void setDeptIdList(ArrayList deptIdList) {
		this.deptIdList = deptIdList;
	}


	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Boolean getIsSenior() {
		return isSenior;
	}

	public void setIsSenior(Boolean isSenior) {
		this.isSenior = isSenior;
	}

	public Boolean getIsBoss() {
		return isBoss;
	}

	public void setIsBoss(Boolean isBoss) {
		this.isBoss = isBoss;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<EmpRoles> getRoleArray() {
		return roleArray;
	}

	public void setRoleArray(ArrayList<EmpRoles> roleArray) {
		this.roleArray = roleArray;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	@Override
	public String toString() {
		return "Emp [userId=" + userId + ", userName=" + userName + ", deptIdList=" + deptIdList + ", position="
				+ position + ", isSenior=" + isSenior + ", isBoss=" + isBoss + ", avatar=" + avatar + ", email=" + email
				+ ", roleArray=" + roleArray + ", isAdmin=" + isAdmin + ", mobile=" + mobile + ", isLeaderInDepts="
				+ isLeaderInDepts + ", leaderDeptIdList=" + leaderDeptIdList + "]";
	}


	

	

	
	
}
