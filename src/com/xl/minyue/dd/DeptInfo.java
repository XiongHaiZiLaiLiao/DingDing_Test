package com.xl.minyue.dd;

public class DeptInfo {
	private String deptId;
	private String deptName;
	private String parentId;
	public DeptInfo(String deptId, String deptName, String parentId) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.parentId = parentId;
	}
	
	
	
	public DeptInfo() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	@Override
	public String toString() {
		return "DeptInfo [deptId=" + deptId + ", deptName=" + deptName + ", parentId=" + parentId + "]";
	}
	
	

}
