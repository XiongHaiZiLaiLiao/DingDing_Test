package com.xl.minyue.dd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Test_1 {
	static final String corpId="ding1c1369684efa555b35c2f4657eb6378f";
	static final String corpsecret="Y5H-jC7Rf4MVZuIWq-O_jvUlJphAKebgNByZrrwuAMwglM0fVvZDFa994-WxHfna";

	public Test_1() {
		super();
		// TODO Auto-generated constructor stub
	}

	//eclipse中new一个对象,前面的补齐代码是哪个快捷键   Shift+Alt+L
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("钉钉,你好!!!!");
		//String res=getAccess_token();
		//System.out.println(res+"--------res---------");
		//getSonDeptIdList(true);
		//getDeptIdList(true);
		//getDeptDetail("64400743");
		//getEmpListByDeptId("64400743");
		getEmpInfo("1841096829766472");
		
	}
	
	//获取人员的详情
	public static Emp getEmpInfo(String userid) {
		Emp emp = new Emp();
		String access_token=getAccess_token();
		String url="https://oapi.dingtalk.com/user/get?access_token="+access_token+"&userid="+userid;	
		String result =getData(url);
		if(result !=null) {
			String s_1=null;
			String s_2=null;
			String s_3=null;
			emp.setUserId(userid);
			if(result.indexOf("active") !=-1) {
				int num2=result.indexOf("active");
				s_1 = result.substring(num2-1,result.length()-1);
			}
			if(result.indexOf("roles") !=-1) {
				int num2=result.indexOf("roles");
				s_2 = result.substring(num2-1,result.length()-1);
			}
			String roles=s_2.substring(0, s_2.length()-s_1.length()-1);
			//第一部分 s_3
			s_3=result.substring(1, result.length()-1-s_2.length());
			//System.out.println(s_3+"s_3s_3s_3s_3s_3s_3");
			String[] arr1=s_3.split(",");
			String s_4=null;
			String s_5=null;
			for (int i = 0; i < arr1.length; i++) {
				String a1 = arr1[i];
				if(a1.indexOf("position") !=-1) {
					int num2=a1.indexOf(":");
					s_4 = a1.substring(num2+1,a1.length());
					emp.setPosition(s_4);
				}
				if(a1.indexOf("department") !=-1) {
					int num2=a1.indexOf(":");
					s_5 = a1.substring(num2+2,a1.length()-1);
					ArrayList<String> deptIdList =new ArrayList<String>(); 
					if(s_5.indexOf(",") !=-1) {
						String[] arr_dept=s_5.split(",");
						if(arr_dept.length>0) {
							for (int j = 0; j < arr_dept.length; j++) {
								String d = arr_dept[j];
								deptIdList.add(d);
							}
						}
					}else {
						deptIdList.add(s_5);
					}
					emp.setDeptIdList(deptIdList);
				}
				if(a1.indexOf("isSenior") !=-1) {
					int num2=a1.indexOf(":");
					String s_6 = a1.substring(num2+1,a1.length());
					if("false".equals(s_6)) {
						emp.setIsSenior(false);
					}else if("true".equals(s_6)) {
						emp.setIsSenior(true);
					}
				}
				if(a1.indexOf("isBoss") !=-1) {
					int num2=a1.indexOf(":");
					String s_6 = a1.substring(num2+1,a1.length());
					if("false".equals(s_6)) {
						emp.setIsBoss(false);
					}else if("true".equals(s_6)) {
						emp.setIsBoss(true);
					}
				}
				if(a1.indexOf("name") !=-1) {
					int num2=a1.indexOf(":");
					String s_6 = a1.substring(num2+1,a1.length());
					emp.setUserName(s_6);
				};
				if(a1.indexOf("avatar") !=-1) {
					int num2=a1.indexOf(":");
					String s_6 = a1.substring(num2+1,a1.length());
					emp.setAvatar(s_6);
				}
				if(a1.indexOf("email") !=-1) {
					int num2=a1.indexOf(":");
					String s_6 = a1.substring(num2+1,a1.length());
					emp.setEmail(s_6);
				}
				if(a1.indexOf("isLeaderInDepts") !=-1) {
					int num3=a1.indexOf("isLeaderInDepts");
					String s7=a1.substring(num3+"isLeaderInDepts\":\"{".length(),a1.length()-2);
					String[] arr3=s7.split(",");
					Boolean s8=false;
					ArrayList<String> arrayList = new ArrayList<String>();
					for (int j = 0; j < arr3.length; j++) {
						String r3 = arr3[j];
						if(r3.endsWith(":true")) {
							arrayList.add(r3.substring(0, r3.length()-":true".length()));
							s8=true;
						}
					}
					emp.setLeaderDeptIdList(arrayList);
					emp.setIsLeaderInDepts(s8);
				}
			}
			//第二部分s_1
			//System.out.println(s_1+"s_1s_1s_1s_1s_1");
			String[] arr4=s_1.split(",");
			for (int i = 0; i < arr4.length; i++) {
				String r4 = arr4[i];
				if(r4.indexOf("isAdmin") !=-1) {
					if(r4.endsWith(":true")) {
						emp.setIsAdmin(true);
					}else if(r4.endsWith(":false")) {
						emp.setIsAdmin(false);
					}
				};
				if(r4.indexOf("mobile") !=-1) {
					int num2=r4.indexOf(":");
					String s_6 = r4.substring(num2+1,r4.length());
					emp.setMobile(s_6);
				}
			}
			//第三部分角色roles
			if(roles.contains("roles")) {
				int num5=roles.indexOf("roles");
				String role_s=roles.substring(num5+"roles".length()+3, roles.length()-1); 
				//System.out.println(role_s+"role_srole_srole_s");
				String[] arr_role=role_s.split(",");
				String roleId=null;
				String groupName=null;
				String roleName=null;
				ArrayList<EmpRoles> roleList = new ArrayList<>();
				for (int i = 0; i < arr_role.length; i++) {
					String role = arr_role[i];
					if(role.indexOf("id") !=-1) {
						int num2=role.indexOf(":");
						roleId = role.substring(num2+1,role.length());
					};
					if(role.indexOf("name") !=-1) {
						int num2=role.indexOf(":");
						roleName = role.substring(num2+1,role.length());
					};
					if(role.indexOf("groupName") !=-1) {
						int num2=role.indexOf(":");
						groupName = role.substring(num2+1,role.length()-1);
						EmpRoles empRoles = new EmpRoles();
						empRoles.setRoleId(roleId);
						empRoles.setRoleName(roleName);
						empRoles.setGroupName(groupName);
						roleList.add(empRoles);
						roleId=null;
						groupName=null;
						roleName=null;
					}
				}
				emp.setRoleArray(roleList);
			}
		}
		System.out.println(emp+"emp_emp_emp_emp_emp_emp_第一,二,三部分");
		return emp;
		
	}
	
	//获取部门中的人员列表
	public static List getEmpListByDeptId(String deptid) {
		List<Emp> list=new ArrayList();
		String access_token=getAccess_token();
		String url="https://oapi.dingtalk.com/user/simplelist?access_token="+access_token+"&department_id="+deptid;	
		String result =getData(url);
		System.out.println(result+"_______result_____");
		String a="{\"userlist\":";
		String b=",\"errmsg\":\"ok\",\"errcode\":0}";
		if(result !=null) {
			String c=result.substring(a.length(), result.length()-b.length());
			System.out.println(c);
			String[] arr2=c.split(",");
			String name=null;
			for (int i = 0; i < arr2.length; i++) {
				String de = arr2[i];
				if(de.indexOf("name") !=-1) {
					int a1=de.indexOf(":");
					String id2=de.substring(a1+1);
					name=id2;
				};
				if(de.indexOf("userid") !=-1) {
					int a2=de.indexOf(":");
					String userid=de.substring(a2+1,de.length()-1);
					Emp emp=new Emp();
					emp.setUserId(userid);
					emp.setUserName(name);
					list.add(emp);
					name=null;
				};
			}
		}
		System.out.println(list+"<-----------list");
		return list;
		
	}
	
	//获取部门详情
	public static deptDetail getDeptDetail(String deptid) {
		String access_token=getAccess_token();
		String url="https://oapi.dingtalk.com/department/get?access_token="+access_token+"&id="+deptid;	
		String result =getData(url);
//		System.out.println(result+"_______deptDetail_____");
		deptDetail detail=new deptDetail();
		
		if(result !=null) {
			String[] arr2=result.split(",");
			detail.setDeptId(deptid);
			for (int i = 0; i < arr2.length; i++) {
				String de = arr2[i];
				if(de.indexOf("parent") !=-1) {
					int a=de.indexOf(":");
					String id2=de.substring(a+1);
					detail.setParentId(id2);
				};
				if(de.indexOf("deptManager") !=-1) {
					int a=de.indexOf(":");
					String id2=de.substring(a+1);
					detail.setDeptMan(id2);
				};
				if(de.indexOf("name") !=-1) {
					int a=de.indexOf(":");
					String name=de.substring(a+1);
					detail.setDeptName(name);
				};
			}
		}
		System.out.println(detail);
		return detail;
		
	}
	
	public static String getData(String url) {
		//System.out.println(url+"--url-------");
		String result = "";
		BufferedReader in = null;
		try {
			URL getUrl = new URL(url);
			URLConnection connection = getUrl.openConnection();
			connection.setRequestProperty("Charset", "UTF-8");
			connection.setRequestProperty("accept", "*/*");
			connection.connect();
			in = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
			//这里如果不加编码方式，解析请求回来的json可能中文乱码报错
			
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
				//System.out.println(in.readLine()+"========"+result);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
	
	//获取access_token
	public static String getAccess_token() {
		String access_token="";
		String url="https://oapi.dingtalk.com/gettoken?corpid="+corpId+"&corpsecret="+corpsecret;
		String result =getData(url);
		//System.out.println("111111111111111111111--->"+result);
		String res=result;
		res=res.substring(1,res.length()-1);
		String[] arr=res.split(",");
		for (int i = 0; i < arr.length; i++) {
			String s_1 = arr[i];
			if(i==2) {
				//System.out.println(s_1.indexOf(":"));
				int num=s_1.indexOf(":");
				String s2=s_1.substring(num+1);
				access_token=s2.substring(1,s2.length()-1);
			}
		}
		return access_token;
	}
	
	// 获取子部门id列表
	public static ArrayList getSonDeptIdList(boolean fetch_child) {
		String access_token=getAccess_token();
		String url="https://oapi.dingtalk.com/department/list_ids?access_token="+access_token+"&id=1"+"&fetch_child="+fetch_child;	
		String result =getData(url);
		result=result.substring(1,result.length()-1);
		String e=",\"errmsg\":\"ok\",\"errcode\":0";
		//System.out.println(result.endsWith(e));
		boolean b=result.endsWith(e);
		int num =result.indexOf(":");
		System.out.println(num);
		ArrayList arr2=new ArrayList();
		if(b) {
			String temp=result.substring(num+1,result.length()-e.length());
			//System.out.println(temp);
			temp=temp.substring(1,temp.length()-1);
			String[] arr=temp.split(",");
			for (int i = 0; i < arr.length; i++) {
				String s_1 = arr[i];
				arr2.add(s_1);
			}
		}
		System.out.println(arr2+"<-------son_deptId_list");
		return arr2;
		
	}
	
	//获取部门信息(id,name,parentid)列表
	public static List getDeptIdList(boolean fetch_child) {
		String access_token=getAccess_token();
		String url="https://oapi.dingtalk.com/department/list?access_token="+access_token+"&id=1"+"&fetch_child="+fetch_child;	
		String result =getData(url);
		System.out.println(result);
		int num_1="{\"errmsg\":\"ok\",\"department\":".length();
		String e=",\"errcode\":0}";
		if(result.endsWith(e)) {
			String s1=result.substring(num_1, result.length()-e.length());
			s1=s1.substring(1, s1.length()-1);
			System.out.println(s1+"<-----s1");
			String[] s2=s1.split(",");
//			System.out.println(s2);
			List<DeptInfo> arr2=new ArrayList();
			String id = null;
			String name = null;
			String parentId = null;
			for (int i = 0; i < s2.length; i++) {
				String dept = s2[i];
				
				if(dept.indexOf("id") !=-1) {
					int a=dept.indexOf(":");
					String id2=dept.substring(a+1);
					if(id==null) {
						id=id2;		
//						System.out.println(id+"<------id--"+i);
					}else {
						parentId=id2;
//						System.out.println(parentId+"<---parentId-----"+i);
					}
					continue;
				}
				if(dept.indexOf("name") !=-1) {
					int a=dept.indexOf(":");
					name=dept.substring(a+1);
//					System.out.println(name+"<-----name---"+i);
					continue;
				}
				if(dept.indexOf("autoAddUser") !=-1) {
					DeptInfo dd=new DeptInfo();
					dd.setDeptId(id);
					dd.setDeptName(name);
					dd.setParentId(parentId);
					System.out.println(dd+"<------dd"+i);
					arr2.add(dd);
					id = null;
					name = null;
					parentId = null;
					continue;
				}
			}
			System.out.println(arr2+"<-------deptId_list");
			return arr2;
		}
		
		return null;
		
	}
	
	
}
